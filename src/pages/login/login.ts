import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { SearchPage } from './../search/search';
import { TabsPage } from './../tabs/tabs';
import { RegisterPage } from './../register/register';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as WC from 'woocommerce-api';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage/dist/storage';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import 'rxjs/add/operator/map';
import { MainCategoriesPage } from '../main-categories/main-categories';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  WooCommerce   : any;
  userData      : any = {};
  RegisterPage  = RegisterPage;
  searchPage  = SearchPage;
  mainCategoriesPage = MainCategoriesPage;

  constructor(public navCtrl: NavController,  public http: Http, public storage: Storage, public toastCtl: ToastController, public menuCtrl: MenuController) {
    this.WooCommerce = WC({
      url: 'https://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    console.log('login data', this.userData.username, this.userData.password)
    //let regExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      console.log('login user pass');
    //   this.http.get('https://koomarket.com/api/auth/generate_auth_cookie/?username='+this.userData.username+'&password='+this.userData.password).
    //   then((response) => {
    //     console.log('response');
    //     console.log(response);
        
    //           if(response.data.user){
    //             this.storage.set('userData',response);
    //             let toast = this.toastCtl.create({
    //               message: 'خوش آمدید' + response.data.user.displayname,
    //               duration: 3000,
    //               position: 'top'
    //             });
    //             toast.present();
        
    //           }else{
    //             let toast = this.toastCtl.create({
    //               message: 'مشکلی وجود دارد',
    //               duration: 3000,
    //               position: 'top'
    //             });
    //             toast.present();
    //           }

    //   }).catch((error) => {
    //     console.log('this is the problem',error);
    // });
    // this.http.get('https://koomarket.com/api/auth/generate_auth_cookie/?username='+this.userData.username+'&password='+this.userData.password).subscribe(data => {
    //       console.log('response');
    //     console.log(data);
    //           var response = JSON.parse(data);
    //           if(response.user){
    //             this.storage.set('userData',response);
    //             let toast = this.toastCtl.create({
    //               message: 'خوش آمدید' + response.data.user.displayname,
    //               duration: 3000,
    //               position: 'top'
    //             });
    //             toast.present();
        
    //           }else{
    //             let toast = this.toastCtl.create({
    //               message: 'مشکلی وجود دارد',
    //               duration: 3000,
    //               position: 'top'
    //             });
    //             toast.present();
    //           }
    // })


    this.http.get('https://koomarket.com/api/auth/generate_auth_cookie/?username='+this.userData.username+'&password='+this.userData.password).map(res => res.json()).subscribe(data => {
      console.log('response');
      console.log(data);
      if(data.user){
        this.storage.set('userData',data.user);
        this.storage.set('logged',true);
        let toast = this.toastCtl.create({
        message: 'خوش آمدید' + data.user.displayname,
        duration: 3000,
        position: 'top'
        });
        toast.present();
        this.navCtrl.push(TabsPage);
      }else{
        let toast = this.toastCtl.create({
        message: 'مشکلی وجود دارد',
        duration: 3000,
        position: 'top'
        });
        toast.present();
      }
  });

  }

  gotoCategories(){
    this.navCtrl.push(this.mainCategoriesPage);
  }

  openMenu(){
    this.menuCtrl.open();
  }

}

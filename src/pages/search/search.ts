import { MainProvider } from './../../providers/main/main';
import { Platform } from 'ionic-angular/platform/platform';
import { Keyboard } from '@ionic-native/keyboard';
import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as WC from 'woocommerce-api';
import { ProductPage } from '../product/product';
import { Storage } from '@ionic/storage';
import { MainCategoriesPage } from '../main-categories/main-categories';
import { Events } from 'ionic-angular/util/events';
import { Tabs } from 'ionic-angular/components/tabs/tabs';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  products: any[];
  WooCommerce: any;
  productPage = ProductPage;
  Arr = Array;
  searchPage  = SearchPage;
  mainCategoriesPage = MainCategoriesPage;
  canLoadMore: Boolean = true;
  search: String = '';
  searching = false;
  page = 0;

  constructor(public navCtrl: NavController, private platform: Platform, public navParams: NavParams, public zone: NgZone, public storage: Storage, public events: Events, public keyboard: Keyboard, private mainService:MainProvider) {

    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });

    this.keyboard.hideKeyboardAccessoryBar(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
    this.zone.runTask(() => {this.getNewProducts()} );
  }

  ionViewWillEnter(){
    this.platform.registerBackButtonAction(() => {
      if (this.navCtrl.length() == 1){
        var t: Tabs = this.navCtrl.parent;
        t.select(0); 
      }else{
        this.navCtrl.pop();
      }
    });
  }

  incremment(number){
    return ++number;
  }

  getNewProducts(){
    this.searching = true;
    this.WooCommerce.getAsync("products").then((data) => {
      console.log("new Products",JSON.parse(data.body));
      this.products = JSON.parse(data.body);

      this.products.forEach(product => {
        product.count = 1;
        this.storage.get("individual-fav").then((val) => {
          val.forEach(element => {
            console.log("loading favorites", element, product);
            if(element.id == product.id)
            product.favorit = true;
          });
         
        })
      });
      this.canLoadMore = true;
      this.searching = false;
    }, (err) => {
      console.log(err);
    })
    
  }


  addToFavorites(sale) {
    sale.favorit = true;
    var exists = false;
    console.log(sale.favorit);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach(element => {
            if(sale.id == element.id && !exists){

              console.log(sale.id + "==" + element.id);
              exists = true;
              

            }
          });

          if(!exists){
            val.push(sale);
            console.log("val",val);
            this.storage.set("individual-fav",val);
          }
          
        // }
      }
      
    })
  }

  deleteFromFavorites(sale) {
    sale.favorit = false;
    var exists = false;
    console.log(sale.favorit);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach((element, index) => {
            if(sale.id == element.id && !exists){
              
              val.splice(index, 1)
              this.storage.set("individual-fav", val);

            }
          });
          
        // }
      }
      
    })
  }

  searchFor(){
      this.searching = true;
      let ind_fav = [];
      // var searchin_phrase = this.search.replace(' ','+');
      this.storage.get("individual-fav").then((val) => {
        ind_fav = val;
      })
      this.mainService.searchProducts(this.search, this.page, (err, products)=> {
        products.forEach(element => {
          element.count = 1;
          ind_fav.forEach((fav) => {
            if(element.id == fav.id){
              element.favorit = true;
            }
          })
            this.products.push(element)
        });
        if(products.length < 25)
          this.canLoadMore = false;
        this.searching = false;
        this.page += 1;
      });
  }

  loadMore(infiniteScroll){
    if(this.canLoadMore){
      let ind_fav = [];
      // var searchin_phrase = this.search.replace(' ','+');
      this.storage.get("individual-fav").then((val) => {
        ind_fav = val;
      })
      this.mainService.searchProducts(this.search, this.page, (err, products)=> {
        products.forEach(element => {
          element.count = 1;
          ind_fav.forEach((fav) => {
            if(element.id == fav.id){
              element.favorit = true;
            }
          })
          setTimeout(() => {
            this.products.push(element)
          }, 200);
        });
        if(products.length < 25)
          this.canLoadMore = false;
        this.searching = false;
        infiniteScroll.complete();
        this.page += 1;
      });
    }
  }

  refresh(refresher){
    console.log(event);
    this.page = 0;
    this.products = [];
    this.searchFor();
  }

  addToCart( item ){
    this.mainService.addToCart(item);
  }

  // addToCart( item ){
  //   console.log("adding to cart");
  //   var exists = false;
  //   this.storage.get("cartItems").then(( val ) => {
  //     console.log("cartItems",val);
  //     val.forEach((element) => {
  //       console.log(item.id + "&&" + element.id);
  //       if( element.id == item.id && !exists ){
  //         console.log(item.id + "==" + element.id);
  //         exists = true;
  //         element.count = item.count;
  //       }
  //     });

  //     if( exists ){
  //       console.log("adding to cart exists");
  //       this.storage.set("cartItems", val);
  //     }else{
  //       this.events.publish('functionCall:addedToCart');
  //       console.log("not adding to cart");
  //       val.push(item);
  //       this.storage.set("cartItems", val);
  //     }
  //   });
  // }

  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

  gotoCategories(){
    this.navCtrl.push(this.mainCategoriesPage);
  }

  onSubmit(){
    this.page = 0;
    this.products = [];
    this.keyboard.close();
    this.searchFor();
  }
}

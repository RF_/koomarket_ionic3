import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
/**
 * Generated class for the CreateGroupeModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-create-groupe-modal',
  templateUrl: 'create-groupe-modal.html',
})
export class CreateGroupeModalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateGroupeModalPage');
  }

  closeModal(event) {
    this.viewCtrl.dismiss();
  }

}

import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AddAddressPage } from './../add-address/add-address';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage/dist/storage';
import * as WC from 'woocommerce-api';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Events } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { Tabs } from 'ionic-angular/components/tabs/tabs';
import { TabsPage } from '../tabs/tabs';



/**
 * Generated class for the AddressesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-addresses',
  templateUrl: 'addresses.html',
})
export class AddressesPage {

  address:      any;
  addresses:    any[] = [];
  addressPage = AddAddressPage;
  user:         any;
  WooCommerce:  any;
  method:       any;
  note:any;
  popover : PopoverController;
  methodCondition = 0;
  tabs = TabsPage;
  loader = this.loading.create({
    content: "در حال بارگزاری",
  });


  paymentMethods = [{ method_id: "WC_ZPal", method_title: "پرداخت از طریق کارت بانکی"},
  { method_id: "cod", method_title: "پرداخت وجه هنگام تحویل درب منزل"}];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public toastCtrl: ToastController, public inAppBrowser: InAppBrowser, public loading: LoadingController, public events: Events, public actionSheetCtrl: ActionSheetController, public menuCtrl: MenuController) {

    this.method = this.paymentMethods[1];

    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressesPage');
  }


  ionViewWillEnter(){
    // this.loader.present();
    this.storage.get('userData').then((val) => {
      console.log('user',val);
      this.user = val;
      if(!val.shipping ){
        this.WooCommerce.getAsync('customers/'+val.id).then(data => {
          console.log('input data',data);
          if(JSON.parse(data.body).shipping){
            this.storage.set('userData', JSON.parse(data.body));
            // this.addresses = [JSON.parse(data.body).shipping.address_1];
            [JSON.parse(data.body).shipping.address_1].forEach(element => {
              this.addresses.push({address: element, selected: false})
            })
            
          }
        })
      }else{
        if(val.shipping.address_1)
          this.addresses.push({address: val.shipping.address_1, selected: false})
        if(val.shipping.address_2)
          this.addresses.push({address: val.shipping.address_2, selected: false})
      }
      if(val.addresses){
        val.addresses.forEach(element => {
          this.addresses.push({address: element, selected: false});
        });
      }
      // this.loader.dismiss();
    })
  }

  isItOkay(){
    console.log("is it okey");
    let actionSheet = this.actionSheetCtrl.create({
      title: 'آیا ادامه می دهید؟',
      buttons: [
        {
          text: 'بله',
          handler: () => {
            this.next();
            console.log('Archive clicked');
          }
        },{
          text: 'خیر',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  next(){
      //Go to Payment
      if(this.method && this.address){
        this.presentLoading();
        var orderItems = [];
        this.storage.get('cartItems').then(data => {
          data.forEach(element => {
            orderItems.push({product_id: element.id, quantity: element.count, price: element.price});
            console.log('paying',this.method, this.address, orderItems);
            var data = {
              "payment_method": this.method.method_id,
              "payment_method_title": this.method.method_title,
              "set_paid": false,
              "billing": {
              "first_name": this.user.billing.first_name,
              "last_name": this.user.billing.last_name,
              "address_1": this.address,
              "address_2": this.user.billing.address_2,
              "city": this.user.billing.city,
              "state": this.user.billing.state,
              "postcode": this.user.billing.postcode,
              "country": this.user.billing.country,
              "email": this.user.billing.email,
              "phone": this.user.billing.phone
            },
            "shipping": {
              "first_name": this.user.shipping.first_name,
              "last_name": this.user.shipping.last_name,
              "address_1": this.address,
              "address_2": this.user.shipping.address_2,
              "city": this.user.shipping.city,
              "state": this.user.shipping.state,
              "postcode": this.user.shipping.postcode,
              "country": this.user.shipping.country
            },
            "customer_id": this.user.id || '',
            "line_items": orderItems,
            "shipping_lines": [
              {
                "method_id": "flat_rate",
                "method_title": "Flat Rate",
                }
              ]
              
            }
            var orderData = data;
            this.WooCommerce.postAsync('orders', orderData).then(data => {
              var body = JSON.parse(data.body);
              console.log('response of order', body);
              let toast = this.toastCtrl.create({
                message: 'در حال ارسال محصولات',
                duration: 3000,
                position: 'top'
                });
              toast.present();
              if(this.method.method_id == "WC_ZPal"){
                // const browser = this.inAppBrowser.create('http://koomarket.com/checkout/order-pay/'+body.id+"/?key="+body.order_key);
                this.inAppBrowser.create('http://koomarket.com/checkout/order-pay/'+body.id+"/?key="+body.order_key);
              }
              this.storage.set('cartItems',[]);
              this.events.publish('functionCall:setCartCount');
              // var status = {
              //   status: 'pending'
              // }
              // this.WooCommerce.putAsync('orders/'+JSON.parse(data.body).id, status ).then(val => {
              //   console.log('set to pending',JSON.parse(data.body),val);
              // })
            });
          });
        })
    }else{
      console.log('set address and method');
      let toast = this.toastCtrl.create({
        message: 'لطفا آدرس و نوع پرداخت را انتخاب کنید',
        duration: 3000,
        position: 'top'
        });
      toast.present();
    }
  }

  presentLoading(){
    let loader = this.loading.create({
      content: "در حال بارگزاری",
      duration: 10000
    });
    loader.present();
  }
  selectAddress(address) {
    this.address = address.address;
    this.addresses.forEach(element => {
      element.selected = false;
    })
    address.selected = true
  }
  selectMethod(method){
    this.method = method;
    if(method.method_id == 'cod')
      this.methodCondition = 0;
    else
      this.methodCondition = 1;
  }

  goback() {
    this.navCtrl.pop();
    console.log('Click on button Test Console Log');
 }
 openMenu(){
  this.menuCtrl.open();
}

gotoCart (){
  this.navCtrl.pop();
  var t: Tabs = this.navCtrl.parent;
      t.select(2);
}

gotoSearch (){
  this.navCtrl.pop();
  var t: Tabs = this.navCtrl.parent;
      t.select(3);
}
}

import { SubCategoriesPage } from './../sub-categories/sub-categories';
import { SearchPage } from './../search/search';
import { CartPage } from './../cart/cart';
import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { FavoritesPage } from './../favorites/favorites';
import { Keyboard } from '@ionic-native/keyboard';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import { Events, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  
  homeRoot = HomePage;
  favoritesRoot = FavoritesPage;
  // categoriesRoot= MainCategoriesPage;
  searchPage = SearchPage;
  cartPage      = CartPage;
  subCategoriesPage = SubCategoriesPage;
  subCategoriesParams = {steps: [{id: 178, name: "سوپر مارکت"}]};
  show    = true;
  cartCount = 0;
  tab = 0;
  index = 0;


  constructor(navParams: NavParams, public nav: NavController, public platform: Platform, private keyboard: Keyboard, public storage : Storage, public events : Events) {
    this.keyboard.onKeyboardShow().subscribe(()=>{this.show=false});
    this.keyboard.onKeyboardHide().subscribe(()=>{this.show=true});
    this.setCount();

    this.tab = navParams.get('tab');

    if(this.tab){
      this.index = this.tab;
    }

      this.events.subscribe('functionCall:addedToCart', (id) => {
        this.cartCount += 1;
      });

      this.events.subscribe('functionCall:removeFromCart', eventData => { 
        this.delete();
      });

      this.events.subscribe('functionCall:setCartCount', () => { 
        this.setCount();
      });

      this.events.subscribe('functionCall:addToFavorit', eventData => { 
        this.setCount();
      });

      this.events.subscribe('functionCall:setStep', eventData => { 
        this.subCategoriesParams.steps = eventData;
      });


      // Observable.forkJoin(
      //   storage.get('cartItems')
      // ).subscribe(function(){
      //   storage.get('cartItems').then(val => {
      //     console.log('new items in tabs', val);
      //     this.cartCount = val.length;
      //   });
      // })

      // storage.get('cartItems').then(val => {
      //   console.log('new items',val);
      //   this.cartCount = val.length;
      // })

  }


  setCount(){
    this.storage.get('cartIDs').then(val => {
      this.cartCount = val.length;
    });
  }

  delete(){
    this.cartCount -= 1;
  }

  doubleClick(){
    console.log('Double click');
  }

}


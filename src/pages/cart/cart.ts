import { Platform } from 'ionic-angular/platform/platform';
import { Dialogs } from '@ionic-native/dialogs';
import { SearchPage } from './../search/search';
import { LoginPage } from './../login/login';
import { AddressesPage } from './../addresses/addresses';
import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as WC from 'woocommerce-api';
import { Tabs } from 'ionic-angular/components/tabs/tabs';
import { Events } from 'ionic-angular/util/events';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  addressPage = AddressesPage;
  cartItems: any[];
  WooCommerce: any;
  searchPage = SearchPage;
  totalPrice = 0;
  loading    = true;
  Arr = Array;

  constructor(public navCtrl: NavController,private platform:Platform, public navParams: NavParams, public storage: Storage, public zone: NgZone,  public events : Events, public dialogs: Dialogs) {

    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });
    // this.zone.runTask(() => {this.getCartItems(null)} );
    
  }

  ionViewWillEnter(){
    this.platform.registerBackButtonAction(() => {
      if (this.navCtrl.length() == 1){
        var t: Tabs = this.navCtrl.parent;
        t.select(0); 
      }else{
        this.navCtrl.pop();
      }
    });
    this.totalPrice = 0
    this.zone.runTask(() => {this.getCartItems(null)} );
  }

  //Getting cart items to and saving to variable
  getCartItems(refresher){
    this.totalPrice = 0
    this.storage.get('cartItems').then((val) => {

      this.cartItems = val;

      this.cartItems.forEach((element, idx, array) => {
        console.log("Stock_quant", element.stock_quantity);
        // if(element.stock_quantity == null){
          element.stock_quantity = 5000;
        // }
        if(element)
        this.WooCommerce.getAsync("products/"+element.id).then((data) => {
          console.log("product "+element.id, data);
          var temp = JSON.parse(data.body);
          // if(temp.stock_quantity != null)

          if( temp.stock_quantity != null  && temp.price != null && temp.regular_price != null && temp.price_html !=null){
            element.stock_quantity  = temp.stock_quantity
            element.price           = temp.price;
            element.regular_price   = temp.regular_price;
            element.price_html      = temp.price_html;
            console.log("cart", val);
            this.storage.set('cartItems', val);
          }
          this.totalPrice += (element.count * element.price)
          if(idx == array.length-1)
            this.loading = false;
        });
      });
      // this.loading = false;
      if(refresher)
        refresher.complete();
    });
  }

  more(item){
    if(item.count < item.stock_quantity)
      item.count = parseInt(item.count) + 1;
    // else

    // this.storage.set("cartItems", this.cartItems);
    // this.totalPrice = 0;
    // this.cartItems.forEach(element => {
    //   this.totalPrice += (element.count * element.price)
    // });
  }
  less(item){
    if(item.count > 0)
      item.count -= 1
    // this.storage.set("cartItems", this.cartItems);
    // this.totalPrice = 0;
    // this.cartItems.forEach(element => {
    //   this.totalPrice += (element.count * element.price)
    // });
  }

  deleteItem(index, price, count){
    this.dialogs.confirm("آیا می خواهید این محصول را پاک کنید؟","پاک کردن").then((i) => {
      if(i == 1){
        console.log('Dialog dismissed');
        this.totalPrice -= (count * price)
        this.cartItems.splice(index, 1);
        this.storage.set('cartItems', this.cartItems);
        this.events.publish('functionCall:removeFromCart');
      }
      
    }).catch((e)=> console.log('Error displaying dialog', e));
  }

  onSelectChange(item){
    console.log("changed");
    this.totalPrice = 0;
    this.cartItems.forEach(element => {
      this.totalPrice += (element.count * element.price)
    });
    if(item.count < 1)
      item.count = 1;
    if(item.count > item.stock_quantity)
      item.count = item.stock_quantity;
    this.storage.set("cartItems", this.cartItems);
  }
  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }
  ionViewCanEnter(){
    if(undefined!=this.navCtrl.getActive())
    {
      if(this.navCtrl.length()==1)
          return false;
    }
  }

  

  // TODO I should add a function to for each cart item get available count and save to item 

  incremment(number){
    return ++number;
  }

  confirmCart(){
    //TODO: if loged in goto address page otherwise goto login page
    this.storage.get('logged').then((val) => {
      this.storage.get('userData').then((user) => {
        if(val && user){
          this.navCtrl.push(this.addressPage);
        }else{
          this.navCtrl.push(LoginPage);
        }
      })
      console.log('logged',val);
      
    })    
  }
}

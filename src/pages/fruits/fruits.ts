import { SearchPage } from './../search/search';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as WC from 'woocommerce-api';
import { NgZone } from '@angular/core';
import { Tabs } from 'ionic-angular/components/tabs/tabs';



/**
 * Generated class for the FruitsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-fruits',
  templateUrl: 'fruits.html',
})
export class FruitsPage {

  categories: any[];
  WooCommerce: any; 
  searchPage  = SearchPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public zone: NgZone) {

    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });

    this.zone.runTask(() => {this.getCategories()} );

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FruitsPage');
  }

  getCategories(){
    this.WooCommerce.getAsync("products/categories?parent=178").then((data) => {
      console.log("categories_178",JSON.parse(data.body));
      this.categories = JSON.parse(data.body);
    }, (err) => {
      console.log(err);
    })
  }
  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

}

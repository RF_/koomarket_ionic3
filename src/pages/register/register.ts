import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Platform } from 'ionic-angular/platform/platform';
import { Storage } from '@ionic/storage';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import * as WC from 'woocommerce-api';


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  WooCommerce: any;
  newUser  = { isValid: true,
    "email": '',
    "first_name": '',
    "last_name": '',
    "username": '',
    "password": '',
    "addresses": {
      "first_name": '',
      "last_name": '',
      "company": "",
      "address_1": '',
      "city": 'رشت',
      "state": 'گیلان',
      "postcode": '',
      "country": 'ایران',
      "email": '',
      "phone": ''
    }
}

  
  
  constructor(public navCtrl: NavController, public navParams: NavParams,private platform: Platform, public toastCtl: ToastController, public menuCtrl: MenuController, public storage: Storage,private loading: LoadingController) {
    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  ionViewWillEnter(){
   this.platform.setDir('rtl', true);
  }

  ionViewDidLeave(){
    this.platform.setDir('ltr', true);
  }

  register(){

    let loader = this.loading.create({
      content: "در حال ثبت نام",
    });
    loader.present();
    
    var customerData = {
      "email": this.newUser.email,
      "first_name": this.newUser.first_name,
      "last_name": this.newUser.last_name,
      "username": this.newUser.username,
      "password": this.newUser.password,
      "billing": {
        "first_name": this.newUser.first_name,
        "last_name": this.newUser.last_name,
        "company": "",
        "address_1": this.newUser.addresses.address_1,
        "address_2": '',
        "city": this.newUser.addresses.city,
        "state": this.newUser.addresses.state,
        "postcode": this.newUser.addresses.postcode,
        "country": this.newUser.addresses.country,
        "email": this.newUser.email,
        "phone": this.newUser.addresses.phone
      },
      "shipping": {
        "first_name": this.newUser.first_name,
        "last_name": this.newUser.last_name,
        "company": "",
        "address_1": this.newUser.addresses.address_1,
        "address_2": '',
        "city": this.newUser.addresses.city,
        "state": this.newUser.addresses.state,
        "postcode": this.newUser.addresses.postcode,
        "country": this.newUser.addresses.country
    }

  }

  this.WooCommerce.postAsync('customers', customerData).then(data => {
    console.log('response', data);
    if(!JSON.parse(data.body).code){
      console.log('registerd');
      let toast = this.toastCtl.create({
        message: 'خوش آمدید',
        duration: 3000,
        position: 'top'
        });
      toast.present();
      console.log('regitster',data);
      // this.storage.set('userData',data.user);
      //   this.storage.set('logged',true);
    this.navCtrl.setRoot(LoginPage);

    }else{
      console.log('not registerd',JSON.parse(data.body).code);
      let toast = this.toastCtl.create({
        message: JSON.parse(data.body).code,
        duration: 3000,
        position: 'top'
        });
        toast.present();
    }
    loader.dismiss();
    
  })
}

  checkUserEmail(email){
    console.log('input login',email);
    var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!regex.test(email)){
      console.log('email accepted',email);
      this.newUser.isValid = false;
      let toast = this.toastCtl.create({
        message: 'ایمیل غیر قابل قبول است',
        duration: 3000,
        position: 'top'
        });
        toast.present();
        return;
    }

    this.WooCommerce.getAsync('customers?email='+email).then((data) => {
      console.log('register email',data);
      if(JSON.parse(data.body)[0]){
        this.newUser.isValid = false;
        let toast = this.toastCtl.create({
          message: 'ایمیل ثبت شده است',
          duration: 3000,
          position: 'top'
          });
        toast.present();

      }else{
        this.newUser.isValid = true;
      }
    });
  }
}

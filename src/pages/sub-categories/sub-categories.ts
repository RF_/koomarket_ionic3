import { MainProvider } from './../../providers/main/main';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { CartPage } from './../cart/cart';
import { SearchPage } from './../search/search';
import { Component, ViewChild } from '@angular/core';
import { NavController, Platform, Refresher, Content, NavParams } from 'ionic-angular';
import { ProductPage } from '../product/product';
import { Storage } from '@ionic/storage/dist/storage';
import { MainCategoriesPage } from '../main-categories/main-categories';
import { Tabs } from 'ionic-angular/components/tabs/tabs';
import { Events } from 'ionic-angular/util/events';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';


/**
 * Generated class for the SubCategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-sub-categories',
  templateUrl: 'sub-categories.html',
})
export class SubCategoriesPage {

  steps: any[];
  isCat: any;
  subCategories: any[] = [];
  products: any[];
  page = 0;
  productPage = ProductPage;
  searchPage  = SearchPage;
  cartPage    = CartPage;
  mainCategoriesPage = MainCategoriesPage;
  loader : any;
  canLoadMore = true;
  show_filter:boolean = false;
  filters: any[] = [];
  filters_temp: any[] = [];
  
  @ViewChild(Content) content: Content;
  @ViewChild(Refresher) refresher: Refresher;
  // @ViewChild(InfiniteScroll) infinit: InfiniteScroll;
  constructor(public navCtrl: NavController, private platform: Platform, public storage: Storage, public events: Events, public menuCtrl: MenuController, public loading: LoadingController, public mainService: MainProvider, private navParams:NavParams) {
    this.isCat = true;
    // this.platform.registerBackButtonAction(() => {
    //   alert('TEst succseed');
    //   this.navCtrl.pop();
    // })
    
    // this.title = this.steps[this.steps.length-1].title;
    // console.log('2 can go back',this.navCtrl.canGoBack());

    //TODO: ADD register for other pages

    this.steps = [{id: 178, name: "سوپر مارکت"}];

    // this.getCategories();
    // this.getAll(null);

    // this.storage.get("currentCat").then(val => {
    //   this.catId = val;
    //   console.log("inside val",val);
    //   this.getParents(this.catId);
    // })
    

  }

  ionViewDidEnter(){
    this.refresher._top = this.content.contentTop + 'px';
    this.refresher.state = 'ready';
    // this.refresher._onEnd();
    this.refresher._beginRefresh();

    let tempSteps = this.navParams.get('steps');
    if( tempSteps )
      this.steps = tempSteps;
  }

  ionViewWillEnter(){
    this.platform.registerBackButtonAction(() => {
      if (this.navCtrl.length() == 1){
        this.back();
      }else{
        this.navCtrl.pop();
      }
    });
  }

  addToFavorites(sale) {
    sale.favorit = true;
    var exists = false;
    console.log(sale.favorit);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach(element => {
            if(sale.id == element.id && !exists){

              console.log(sale.id + "==" + element.id);
              exists = true;
              

            }
          });

          if(!exists){
            val.push(sale);
            console.log("val",val);
            this.storage.set("individual-fav",val);
          }
          
        // }
      }
      
    })
  }

  deleteFromFavorites(sale) {
    sale.favorit = false;
    var exists = false;
    console.log(sale.favorit);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach((element, index) => {
            if(sale.id == element.id && !exists){
              
              val.splice(index, 1)
              this.storage.set("individual-fav", val);

            }
          });
          
        // }
      }
      
    })
  }

  back(){
    if(this.loader){
      this.loader.dismiss();
      this.loader = null;
    }else if(this.show_filter){
      this.show_filter = false;
    }else if(this.steps.length == 1){
      var t: Tabs = this.navCtrl.parent;
      t.select(0); 
    }else{
      this.steps.splice(this.steps.length-1, 1);
      this.getAll(null, true);
      this.setSteps();
    }
  }

  openFilter(){
    this.show_filter = true;
    this.refresher._top = this.content.contentTop + 'px';
    this.refresher.state = 'completing';
    this.refresher._isEnabled = false;
    // this.infinit.enable(false);
    // this.infinit
  }

  closeFilter(){
    this.show_filter = false;
    this.refresher._top = this.content.contentTop + 'px';
    this.refresher.state = 'inactive';
    this.refresher._isEnabled = true;
  }

  

  getAll(refresher, shouldCat){
    this.isCat = true;
    this.loader = this.loading.create({
      content: "در حال بارگزاری",
    });
    this.loader.present();
    this.page = 0;
    if(shouldCat)
      this.getCategories();

    if(this.steps.length > 2){
      this.isCat = false;
      this.getProducts(this.steps[this.steps.length-1].id, null); 
    }else{
      this.products = [];
    }

    if(refresher)
      setTimeout(refresher.complete(), 2000);
  }



  getCategories(){
    this.mainService.getSubcategoies(this.steps[this.steps.length-1].id, (err, cat) => {
      if(err){
        console.log("asdf");
      }else{
        cat.forEach(element => {
          element.css = "uncheck back-filter";
        });
        this.subCategories = cat;
      }
      if(this.loader && this.isCat){
        this.loader.dismiss();
        this.loader = null;
        this.refresher.state = 'inactive';
        this.refresher._onEnd();
      }
    });
  }
  

  getParents( id ){
    this.loader = this.loading.create({
      content: "در حال بارگزاری",
    });
    this.loader.present();
    this.storage.get('category-cashe').then(val => {
      if(val){
        console.log('storage cat', val)
        val.categories.forEach(element => {
          if(element.parent === id){
            // console.log('first cond');
            // this.getProducts(this.catId);
            this.subCategories.push(element);
          }
        });
        if(this.subCategories.length == 0)
          this.getProducts(id, null);
        else
          this.loader.dismiss();
          this.loader = null;
      }
    });
  };

  getProducts( id, loader ){
    if(this.filters.length > 0){
      // if(this.filters[0].page == 0)
      //     this.products = [];
      this.filters.forEach(element => {
        if(element.hasmore)
        this.mainService.getProductsById(element.id, element.page, (err, products)=>{
          if(products.length < 10)
            element.hasmore = false;

          if(err){
            element.page += 1;
            products.forEach(product => {
              this.products.push(product);
            });
            console.log("error");
          }else{
            element.page += 1;
            products.forEach(product => {
              this.products.push(product);
            });
          }
          if(this.loader){
            this.loader.dismiss();
            this.loader = null;
          }
          if(loader)
            loader.complete();
        })
      });
    }else{
      this.mainService.getProductsById(id, this.page, (err, products) => {
        // if(this.page == 0)
        //   products = [];

        if(err){
          console.log("error");
          products.forEach(element => {
            this.products.push(element);
          });
          // this.products = products;
        }else{
          products.forEach(element => {
            this.products.push(element);
          });
          this.page += 1;
        }
        this.loader.dismiss();
        if(loader)
            loader.complete();
      });
    }
    this.refresher.state = 'inactive';
        this.refresher._onEnd();
  }
  

  backtoSubCategory( cat ){
    this.steps.splice(this.steps.indexOf(cat)+1,this.steps.length);
    this.getAll(null, true);
    this.setSteps();
  }

  addToCart( item ){
    this.mainService.addToCart(item);
  }

  onCategoryClick(item){
    this.steps.push({id: item.id, name: item.name});
    this.getAll(null, true);
    this.setSteps();
  }
  filterTmp(item){
    let tmpObj = item.id;
      let temp = this.filters_temp.indexOf(tmpObj);
      if(temp != -1){
        item.css = "uncheck back-filter";
        this.filters_temp.splice(temp, 1);
      }else{
        item.css = "check back-filter";
        this.filters_temp.push(tmpObj);
      }
  }

  clearTmpFilter(){
    this.filters_temp = [];
    this.subCategories.forEach(element=> {
      element.css = "uncheck back-filter";
    });
  }
  performFilter(){
    this.filters = [];
    this.filters_temp.forEach(element => {
      this.filters.push({id: element, page: 0, hasmore: true});
    });
    this.products = [];
    this.getAll(null, false);
    this.show_filter = false;
  }
  // addToCart( item ){
  //   console.log("adding to cart");
  //   var exists = false;
  //   this.storage.get("cartItems").then(( val ) => {
  //     console.log("cartItems",val);
  //     val.forEach((element) => {
  //       console.log(item.id + "&&" + element.id);
  //       if( element.id == item.id && !exists ){
  //         console.log(item.id + "==" + element.id);
  //         exists = true;
  //         element.count = item.count;
  //       }
  //     });

  //     if( exists ){
  //       console.log("adding to cart exists");
  //       this.storage.set("cartItems", val);
  //     }else{
  //       this.events.publish('functionCall:addedToCart');
  //       console.log("not adding to cart");
  //       val.push(item);
  //       this.storage.set("cartItems", val);
  //     }
  //   });
  // }
  gotoCategories(){
    this.navCtrl.push(this.mainCategoriesPage);
  }

  

  openMenu(){
    this.menuCtrl.open();
  }
  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

  loadMore( loader ){
    this.page += 1;
    this.getProducts(this.steps[this.steps.length-1].id, loader);
    // this.WooCommerce.getAsync("products?category=" + this.catId + "page=" + this.page).then((data) => {
    //   console.log("products",JSON.parse(data.body));
    //   if(JSON.parse(data.body).length < 10)
    //     this.canLoadMore = false;
    //   for(var i = 0; i < JSON.parse(data.body).length ; i++){
    //     data.body.count = 1
    //     this.products.push(data.body[i]);
    //   }
    //   loader.complete();
    // });
  }

  setSteps(){
    this.mainService.setSubCategoryStep(this.steps);
  }

  refresh(refresher){
    this.products = [];
    this.getAll(refresher, true);
  }
}
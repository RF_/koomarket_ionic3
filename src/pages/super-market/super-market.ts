import { MainProvider } from './../../providers/main/main';
import { CartPage } from './../cart/cart';
import { SearchPage } from './../search/search';
import { SubCategoriesPage } from './../sub-categories/sub-categories';
import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { NgZone } from '@angular/core';
import { MainCategoriesPage } from '../main-categories/main-categories';
import { Tabs } from 'ionic-angular/components/tabs/tabs';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the SuperMarketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-super-market',
  templateUrl: 'super-market.html'
})
export class SuperMarketPage {

  categories: any[] = [];
  subCategoriesPage = SubCategoriesPage;
  cartPage = CartPage;
  tabBarElement: any;
  searchPage  = SearchPage;
  mainCategoriesPage = MainCategoriesPage;
  loader = this.loading.create({
    content: "در حال بارگزاری",
  });

  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App,public zone: NgZone, public loading: LoadingController, private mainService: MainProvider) {
    
    this.zone.runTask(() => {this.getCategories()} );
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SuperMarketPage');
  }

  // getCategories(){
  //   this.loader.present();
  //   this.WooCommerce.getAsync("products/categories?parent=178").then((data) => {
  //     console.log("categories_178",JSON.parse(data.body));
  //     this.categories = JSON.parse(data.body);
  //     this.loader.dismiss();
  //   }, (err) => {
  //     this.loader.dismiss();
  //     console.log(err);
  //   })
  // }

  getCategories(){
    this.mainService.getSubcategoies(178, (err, cat) => {
      if(err){
        console.log("asdf");
      }else{
        this.categories = cat;
      }
    })
  };

  gotoSubCategory( itemId, itemTitle ){
    // console.log("id",itemId);
    // // var params = {
    // //   id : itemId,
    // //   steps : [{title:"سوپر مارکت", id: '178'}, {title: itemTitle, id: itemId}]
    // //  };
    // this.storage.set("currentCat", itemId);
    // this.storage.set("steps", [{title:"سوپر مارکت", id: '178'}, {title: itemTitle, id: itemId}]);
    // this.navCtrl.push( this.subCategoriesPage);
    // // console.log('can go back',this.navCtrl.canGoBack());
  }

  gotoCategories(){
    this.navCtrl.push(this.mainCategoriesPage);
  }

  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

  presentLoading(){
    let loader = this.loading.create({
      content: "در حال بارگزاری",
    });
    loader.present();
  
  }
}

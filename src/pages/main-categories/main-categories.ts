import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the MainCategoriesPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-main-categories',
  templateUrl: 'main-categories.html'
})
export class MainCategoriesPage {

  fruitsRoot = 'FruitsPage'
  superMarketRoot = 'SuperMarketPage'


  constructor(public navCtrl: NavController) {}

}

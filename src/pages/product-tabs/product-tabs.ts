import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the ProductTabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-tabs',
  templateUrl: 'product-tabs.html'
})
export class ProductTabsPage {

  reviewsRoot = 'ReviewsPage'
  productInformationRoot = 'ProductInformationPage'
  moreRoot = 'MorePage'


  constructor(public navCtrl: NavController) {}

}

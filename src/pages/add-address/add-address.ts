import { Tabs } from 'ionic-angular/components/tabs/tabs';
import { Component } from '@angular/core';
import {  NavController, NavParams, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage/dist/storage';

/**
 * Generated class for the AddAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-address',
  templateUrl: 'add-address.html',
})
export class AddAddressPage {
  address : String;
  name    : String;
  phone   : String;
  post    : String;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage : Storage, public menuCtrl: MenuController) {
  }

  openMenu(){
    this.menuCtrl.open();
  }
  
  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddAddressPage');
  }

  add(){
    console.log('adding');
    if(this.address && this.name && this.phone && this.post){
      let temp = this.name +"\n"+this.address+"\n"+this.post+"\n"+this.phone;
      console.log(temp);
      this.storage.get('userData').then( val => {
        if(val.addresses){
          console.log(val.address);
          val.addresses.push(temp);
          this.storage.set('userData',val);
          this.navCtrl.pop();
        }else{
          console.log('else');
          val.addresses = [ temp ];
          this.storage.set('userData',val);
          this.navCtrl.pop();
        }
      })
    }
  }
}

import { MainProvider } from './../../providers/main/main';
import { Events } from 'ionic-angular/util/events';
import { CreateGroupeModalPage } from './../create-groupe-modal/create-groupe-modal';
import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { List } from 'ionic-angular/components/list/list';
import { ProductPage } from '../product/product';
import { Tabs } from 'ionic-angular/components/tabs/tabs';

/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {

  // groups: any[];
  individuals: any[];
  Arr = Array
  productPage = ProductPage;

  @ViewChild('indiviuals') individualsList: List;
  constructor(public navCtrl: NavController, private platform: Platform, public navParams: NavParams, public storage: Storage, public modalCtl: ModalController, public zone: NgZone, public events: Events, private mainService: MainProvider) {
    // this.getGroups();
    this.getIndividuals();
  }

  addToCart( item ){
    this.mainService.addToCart(item)
  }

  // addToCart( item ){
  //   console.log("adding to cart");
  //   var exists = false;
  //   this.storage.get("cartItems").then(( val ) => {
  //     console.log("cartItems",val);
  //     val.forEach((element) => {
  //       console.log(item.id + "&&" + element.id);
  //       if( element.id == item.id && !exists ){
  //         console.log(item.id + "==" + element.id);
  //         exists = true;
  //         element.count = item.count;
  //       }
  //     });

  //     if( exists ){
  //       console.log("adding to cart exists");
  //       this.storage.set("cartItems", val);
  //     }else{
  //       this.events.publish('functionCall:addedToCart');
  //       console.log("not adding to cart");
  //       val.push(item);
  //       this.storage.set("cartItems", val);
  //     }
  //   });
  // }

  deleteFromFavorites( i, sale) {
    sale.favorit = false;
    console.log(sale.favorit);
    this.individuals.splice(i, 1);
    this.storage.set("individual-fav", this.individuals);
  }
    

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }
  ionViewWillEnter(){
    this.platform.registerBackButtonAction(() => {
      if (this.navCtrl.length() == 1){
        var t: Tabs = this.navCtrl.parent;
        t.select(0); 
      }else{
        this.navCtrl.pop();
      }
    });
    this.zone.runTask(() => {this.getIndividuals()} );
  }

  // getGroups(){
  //   this.storage.get("groups").then((val) => {
  //     this.groups = val;
  //     console.log("fave",this.groups);
  //   });
  //   console.log("fav-group:",this.groups);
  // }

  // saveGroups(){
  //   this.storage.set("groups", this.groups);
  // }

  getIndividuals(){
    this.storage.get("individual-fav").then((val) => {
      if(val != null){
        this.individuals = val;  
        console.log("favorit indivuakgj:",val);

      }
        else{
          this.individuals = [];
        }

    })
  }

  getAll(refresher){
    this.storage.get("individual-fav").then((val) => {
      if(val != null){
        this.individuals = val;  
        console.log("favorit indivuakgj:",val);

      }
        else{
          this.individuals = [];
        }
        refresher.complete();
    })
  }

  openCreateGroupeModal(){
    let createModal = this.modalCtl.create(CreateGroupeModalPage);
    createModal.present();
  }

  incremment(number){
    return ++number;
  }

  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

}

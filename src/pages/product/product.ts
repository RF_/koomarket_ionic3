import { MainProvider } from './../../providers/main/main';
import { SearchPage } from './../search/search';
import { CartPage } from './../cart/cart';
import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import * as WC from 'woocommerce-api';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular/util/events';
import { Tabs } from 'ionic-angular/components/tabs/tabs';
/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector    : 'page-product',
  templateUrl : 'product.html',
})
export class ProductPage {

  WooCommerce     : any;
  productId       : any;
  product         : any;
  productReviews  : any[];
  productAttributes:any[];
  images          : any[];
  segment         : "att";
  Arr             = Array;
  reviewColor     = "primary";
  infoColor       = "light";
  moreColor       = "light";
  cartPage        = CartPage;
  SearchPage      = SearchPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public menuCtrl : MenuController, public events: Events, private mainService: MainProvider) {

    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });
    



    this.productId = this.navParams.get('id');
    this.getProduct();
    this.getProductReviews();
    this.segment = "att";
    // this.selectInfo();
  }

  // ionViewDidLoad(){
  //   setInterval(()=> {

  //     if(this.productSlides.getActiveIndex() == this.productSlides.length() -1)
  //       this.productSlides.slideTo(0);
  //     else
  //       this.productSlides.slideNext();
  //     if(this.bannerSlide.getActiveIndex() == this.bannerSlide.length()-1)
  //       this.bannerSlide.slideTo(0);
  //     else
  //       this.bannerSlide.slideNext();
  //   }, 3000)
  // }

  getProduct(){
    this.mainService.getProduct(this.productId, (err, product)=>{
      if(err){
        console.log("error");
      }else{
        this.product = product;
        this.images = product.images;
        this.productAttributes = product.attributes;
        product.count = 1;
      }
    })

    // this.WooCommerce.getAsync("products/"+ this.productId).then((val) => {

    //   var temp                = JSON.parse(val.body);
    //   this.product            = temp;
    //   this.images             = temp.images;
    //   this.productAttributes  = temp.attributes;

    //   this.product.count = 1;
      
    //   console.log("product", this.product);
    //   console.log("images", this.images);
    // })
  }

  getProductReviews(){
    this.WooCommerce.getAsync("products/"+ this.productId + '/reviews').then((val) =>{

      console.log('Reviews', val.body);
      this.productReviews = JSON.parse(val.body);
    })
  }
  log(){
    console.log("clicked");
  }

  incremment(number){
    return ++number;
  }

  // addToCart( item ){
  //   console.log("adding to cart");
  //   var exists = false;
  //   this.storage.get("cartItems").then(( val ) => {
  //     console.log("cartItems",val);
  //     val.forEach((element) => {
  //       console.log(item.id + "&&" + element.id);
  //       if( element.id == item.id && !exists ){
  //         console.log(item.id + "==" + element.id);
  //         exists = true;
  //         element.count = item.count;
  //       }
  //     });

  //     if( exists ){
  //       console.log("adding to cart exists");
  //       this.storage.set("cartItems", val);
  //     }else{
  //       this.events.publish('functionCall:addedToCart');
  //       console.log("not adding to cart");
  //       val.push(item);
  //       this.storage.set("cartItems", val);
  //     }
  //   });
  // }

  addToCart( item ){
    this.mainService.addToCart(item);
    
  }
  
  addToFavorites(event, product) {
    product.favorit = true;
    var exists = false;
    console.log(product.favorit);
    console.log("click",event);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach(element => {
            if(product.id == element.id && !exists){

              console.log(product.id + "==" + element.id);
              exists = true;
              

            }
          });

          if(!exists){
            val.push(product);
            console.log("val",val);
            this.storage.set("individual-fav",val);
          }
          
        // }
      }
      
    });
  }

  moree(item){
    if(item.count < item.stock_quantity)
      item.count = parseInt(item.count) + 1;
    // else

    // this.storage.set("cartItems", this.cartItems);
    // this.totalPrice = 0;
    // this.cartItems.forEach(element => {
    //   this.totalPrice += (element.count * element.price)
    // });
  }
  less(item){
    if(item.count > 0)
      item.count -= 1
    // this.storage.set("cartItems", this.cartItems);
    // this.totalPrice = 0;
    // this.cartItems.forEach(element => {
    //   this.totalPrice += (element.count * element.price)
    // });
  }

  onSelectChange(item){
    if(item.count < 1)
      item.count = 1;
    if(item.count > item.stock_quantity)
      item.count = item.stock_quantity;
  }

  deleteFromFavorites(event, sale) {
    sale.favorit = false;
    var exists = false;
    console.log(sale.favorit);
    console.log("click",event);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach((element, index) => {
            if(sale.id == element.id && !exists){
              
              val.splice(index, 1)
              this.storage.set("individual-fav", val);

            }
          });
          
        // }
      }
      
    })
  }

  // selectReview(){
  //   if(this.reviewColor == "primary"){}
  //     // this.reviewColor = "light";
  //     else{
  //       this.rev = true;
  //       this.att = false;
  //       this.more= false;
  //     }
        

  // }

  // selectInfo(){
  //   if(this.infoColor == "primary"){}
  //   // this.reviewColor = "light";
  //   else{
  //     this.rev = false;
  //     this.att = true;
  //     this.more= false;
  //   }
  // }

  // selectMore(){
  //   if(this.moreColor == "primary"){}
  //   // this.reviewColor = "light";
  //   else{
  //     this.rev = false;
  //     this.att = false;
  //     this.more= true;
  //   }
  // }
  openMenu(){
    this.menuCtrl.open();
  }

  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }
}

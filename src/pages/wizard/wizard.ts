import { Storage } from '@ionic/storage/dist/storage';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular/components/slides/slides';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the WizardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-wizard',
  templateUrl: 'wizard.html',
})
export class WizardPage {

  @ViewChild('wizard') wizardSlide: Slides;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage : Storage) {
  }

  goHome() {
    this.storage.set('wizard', true);
    this.navCtrl.setRoot(TabsPage);
  }

  ionViewDidLoad(){
  }

}

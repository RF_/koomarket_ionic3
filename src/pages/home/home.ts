import { MainProvider } from './../../providers/main/main';
import { Platform } from 'ionic-angular/platform/platform';
import { TabsPage } from './../tabs/tabs';
import { SearchPage } from './../search/search';
import { MainCategoriesPage } from './../main-categories/main-categories';
import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Slides } from 'ionic-angular/components/slides/slides';
import { Storage } from '@ionic/storage';
import { Tabs } from 'ionic-angular/components/tabs/tabs';
import { ProductPage } from '../product/product';
import { CartPage } from './../cart/cart';
// import { Events } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {


  // WooCommerce: any;
  newProducts: any[];
  mostSales: any[];
  categories: any[] = [];
  bannerSlides: any[];
  mainCategoriesPage = MainCategoriesPage;any
  tabs = TabsPage;
  productPage = ProductPage;
  searchPage  = SearchPage;
  cartPage  = CartPage;



  Arr = Array;
  @ViewChild('bannerSlides') bannerSlide: Slides;
  @ViewChild('productSlides') productSlides: Slides;
  @ViewChild('MostSalesSlides') mostSlides: Slides;
  constructor(public navCtrl: NavController, public platform: Platform, public storage: Storage, private mainService: MainProvider) {

  //   this.platform.ready().then(() => {
  //     if (this.navCtrl.length() == 1){
  //       this.platform.registerBackButtonAction(() => {
  //         this.isItOkay();
  //       });
  //     }else{
  //       this.navCtrl.pop();
  //     }
  // });

    

    // this.loader.present();

    // this.WooCommerce = WC({
    //   url: 'http://koomarket.com',
    //   consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
    //   consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
    //   wpAPI: true,
    //   version: 'wc/v2'
    // });

    this.getNewProducts();
    this.getMostSales();
    this.getCategories();   
    // this.getVersionNumber();
  }

  ionViewDidLoad(){
    setInterval(()=> {

      if(this.productSlides.getActiveIndex() == this.productSlides.length() -1)
        this.productSlides.slideTo(0);
      else
        this.productSlides.slideNext();
      if(this.bannerSlide.getActiveIndex() == this.bannerSlide.length()-1)
        this.bannerSlide.slideTo(0);
      else
        this.bannerSlide.slideNext();
    }, 3000)
  }

  ionViewDidEnter(){
    let exit = false;
    this.platform.registerBackButtonAction(() => {
      if (this.navCtrl.length() == 1){
        if(exit)
        this.platform.exitApp();
      else{
        this.mainService.showToast("برای خروج دوباره بازگشت را فشار دهید")
        exit = true;
        setTimeout(function(){
          exit = false
        }, 500);
      }
      }else{
        this.navCtrl.pop();
      }
      

    })
  }

  // async getVersionNumber() {
  //   this.version = await this.appVersion.getVersionCode();
  //   console.log("versionNumber", this.version);
  // }


  getAll(refresher){
    this.getNewProducts();
    this.getMostSales();
    this.getCategories();
    refresher.complete();
    

  }
  
  getNewProducts(){
    this.mainService.getNewProducts((err, products) => {
      if(err){
        console.log("sdf");
      }else{
        this.newProducts = products;
      }
    })
    
  };


  getMostSales(){
    this.mainService.getMostSales((err, products)=>{
      console.log("most products", err, products);
      if(err)
      console.log("asdf");
        // setTimeout(function(){
        // }, 3000);
      else
        this.mostSales = products;
    });
  }

  getCategories(){
    this.mainService.getSubcategoies(178, (err, cat) => {
      if(err){
        this.categories = cat;
        console.log("asdf");
      }else{
        this.categories = cat;
      }
    });
  }

  // checkPresence(indi){
  //   return indi == 
  // }
  addToFavorites(event, sale) {
    sale.favorit = true;
    var exists = false;
    console.log(sale.favorit);
    console.log("click",event);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach(element => {
            if(sale.id == element.id && !exists){

              console.log(sale.id + "==" + element.id);
              exists = true;
            }
          });

          if(!exists){
            val.push(sale);
            console.log("val",val);
            this.storage.set("individual-fav",val);
          }
          
        // }
      }
      
    })
  }

  deleteFromFavorites(event, sale) {
    sale.favorit = false;
    var exists = false;
    console.log(sale.favorit);
    console.log("click",event);
    this.storage.get("individual-fav").then((val) => {
      console.log(1);
      if(val != null){
        console.log(2);
        // if(!val.indexOf(sale)){
          console.log("pushing");
          val.forEach((element, index) => {
            if(sale.id == element.id && !exists){
              
              val.splice(index, 1)
              this.storage.set("individual-fav", val);

            }
          });          
      }
      
    })
  }
  
  gotoSubCategory( itemId, itemTitle ){
    console.log("id",itemId);
    if(itemId)
      this.mainService.setSubCategoryStep([{id: 178, name: "سوپر مارکت"},{name: itemTitle, id: itemId}])
    else
      this.mainService.setSubCategoryStep([{id: 178, name: "سوپر مارکت"}])
    var t: Tabs = this.navCtrl.parent;
    t.select(1);
  }

  gotoCart (){
    var t: Tabs = this.navCtrl.parent;
        t.select(2);
  }

  gotoSearch (){
    var t: Tabs = this.navCtrl.parent;
        t.select(3);
  }

  addToCart( item ){
    if(item.stock_quantity)
      this.mainService.addToCart(item);
  }

  // isItOkay(){
  //   let actionSheet = this.actionSheetCtrl.create({
  //     title: 'آیا می خواهید خارج شوید؟',
  //     buttons: [
  //       {
  //         text: 'بله',
  //         handler: () => {
  //           navigator['app'].exitApp();
  //         }
  //       },{
  //         text: 'خیر',
  //         role: 'cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       }
  //     ]
  //   });
  //   actionSheet.present();
  // }
}

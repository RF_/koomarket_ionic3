import { Injectable } from '@angular/core';

@Injectable()
export class GlobalVarsProvider {

  groups: any[];
  cartItems : any[];

  constructor() {
    this.groups = []
  }

  setGroups(groups){
    this.groups = groups;
  }

  getGroups(){
    return this.groups;
  }

  addToGroup(group){
    this.groups.push(group);
  }

  setCart(groups){
    this.cartItems = groups;
  }

  getCart(){
    return this.cartItems;
  }

  addToCart(group){
    this.cartItems.push(group);
  }

}

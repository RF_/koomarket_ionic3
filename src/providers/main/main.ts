import { AppUpdate } from '@ionic-native/app-update';
import { Events } from 'ionic-angular/util/events';
import { ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { differenceInDays } from 'date-fns';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import * as WC from 'woocommerce-api';
import { HTTP } from '@ionic-native/http';
/*
  Generated class for the MainProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MainProvider {
  public message: any = "I'm new here";
  // public categories: any = [];
  private toastInstance = null;
  WooCommerce: any;

  constructor(private toastCtrl: ToastController, private storage: Storage, private network: Network, private events: Events, public http: HTTP, private appUpdate: AppUpdate) {
    console.log('Hello MainProvider Provider');
    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });
    this.initialization();
  }

  private initialization(){
    this.checkConnection();

    this.storage.get('cartItems').then((val) => {
      if( !val )
        this.storage.set('cartItems',[]);
    });

    this.storage.get('cartIDs').then((val) => {
      if( !val )
        this.storage.set('cartIDs',[]);
    });

    this.events.publish('functionCall:setCartCount');
  }


  setMessage(message) {
    this.message = message;
  }

  /**************************** */
  /**********IN APP FUNCS****** */
  /**************************** */
  // get

  addToCart(item){
    if(!item.count)
      item.count = 1;
    console.log("add to cart",0);
    this.storage.get('cartIDs').then(val => {
      console.log("add to cart",val);
      if(val){
        console.log("add to cart",2);
      if(val.indexOf(item.id) == -1){
        console.log("add to cart",3);
        this.showToast('محصول مورد نظر به سبد اضافه شد');
        this.events.publish('functionCall:addedToCart', item.id);
        val.push(item.id);
        this.storage.set('cartIDs', val);
        this.storage.get('cartItems').then(items => {
          items.push(item);
          this.storage.set('cartItems', items);
        })
      }else{
        console.log("add to cart",4);
        this.showToast('محصول در سبد موجود است')
      }
    }else{
      this.showToast('محصول مورد نظر به سبد اضافه شد');
      console.log("add to cart",5);
      this.storage.set('cartIDs', [item.id]);
      this.storage.set('cartItems', [item]);
      this.events.publish('functionCall:addedToCart', item.id);
    }
    })
  }


  //SHOWING TOAST WITH DURATION
  showToast( text: string ){
    if(this.toastInstance){
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 1000,
      position: 'top',
    });
    this.toastInstance.onDidDismiss(()=>{
      this.toastInstance = null;
      console.log('Dismissed toast');
    });
    this.toastInstance.present();
  }


  //SHOWING TOAST WITH CANCEL BUTTON
  private showToastCancel( text: string ){
    if(this.toastInstance){
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      showCloseButton: true,
      position: 'top',
    });
    this.toastInstance.onDidDismiss(()=>{
      this.toastInstance = null;
      console.log('Dismissed toast');
    });

    this.toastInstance.present();
  }

  setSubCategoryStep(step){
    this.events.publish('functionCall:setStep', step);
  }



  //**************************** */
  /********WOO API SECTION****** */
  /***************************** */
  private getCategories(){
    this.storage.get('category-cashe').then((val) => {
      if(val){
        console.log("diffrence",differenceInDays(val.date, new Date()));
        if( differenceInDays(val.date, new Date()) < -3 ){
          this.reachCashe(function(err, list){

          });
        }else
          this.showToast("اتصال به سرور برقرار شد");
          return true;
      }else{
          this.reachCashe(function(err, list){

          });
      }
    });
  }

  private reachCashe(callback){
    this.showToast("در حال اتصال به سرور");
    let tmpArray = [];

    this.http.post("http://89.42.209.153:3002/getCategories", {},{}).then(data => {
      console.log("HTTP",JSON.parse(data.data));
      if(JSON.parse(data.data).err){
        // this.showToast(data.data.err);
        console.log(JSON.parse(data.data.err));
        callback(true, tmpArray);
      }
        
      else{
        tmpArray = JSON.parse(data.data).categories;
        // this.showToast(tmpArray[0].name);
        this.storage.set('category-cashe', {date: new Date(),categories: JSON.parse(data.data).categories});
        callback(false, tmpArray);
      }

    });
    // let hasMore = true;
    // for( let i = 1; i < 7 && hasMore; i++){
    //   if(hasMore)
    //     this.WooCommerce.getAsync('products/categories?per_page=100&page='+i).then((data) => {
    //       if( (JSON.parse(data.body).status == 400) || (JSON.parse(data.body).status == 401) || (JSON.parse(data.body).status == 404) || (JSON.parse(data.body).status == 500) ){
    //         hasMore = false;
    //           callback(true, tmpArray);
    //         }                
    //       console.log("loaded caregories", i,JSON.parse(data.body));
    //       // tmpArray.push(JSON.parse(data.body))
    //       JSON.parse(data.body).forEach(element => {
    //         tmpArray.push(element);
    //       });
    //       setTimeout(() => {
    //         if((JSON.parse(data.body).length < 100 )){
    //           console.log("cashe done", tmpArray);
    //           this.storage.set('category-cashe', {date: new Date(),categories: tmpArray});
    //           hasMore = false;
    //           callback(false, tmpArray);
    //         }
    //       }, 200);
          
    //     }, (err) => {
    //       console.log(err);
    //       callback(true, tmpArray);
    //     });   
    // }  
  }

  getSubcategoies( id, callback ){
    let temp = [];
    // let e : boolean;
    // this.reachCashe(function(err, cats){
    //   cats.forEach(element => {
    //     if(element.parent === id)
    //       temp.push(element);
    //   });
    //   setTimeout(callback(err, temp), 3000);
    // });
    this.storage.get('category-cashe').then(val => {
      if(val){
        console.log('storage cat', val);
        if(differenceInDays(val.date, new Date()) > -1 && val.categories.length > 300){
          console.log("cat storage",0);
          val.categories.forEach(element => {
            if(element.parent === id)
              temp.push(element);
          });
          setTimeout(callback(false, temp), 3000);
        }else{
          console.log("cat storage",1);
          this.reachCashe(function(err, cats){
            cats.forEach(element => {
              if(element.parent === id)
                temp.push(element);
            });
            setTimeout(callback(err, temp), 3000);
          });
        }
        
      }else{
        console.log("cat storage",2);
        this.reachCashe(function(err, cats){
          cats.forEach(element => {
            if(element.parent === id)
              temp.push(element);
          });
          setTimeout(callback(err, temp), 3000);
        });
      }
      
    })
  }

  reachMostSales(callback){
    let temp = [];
    this.WooCommerce.getAsync("reports/top_sellers?period=year").then((data) => {
      console.log("most Sales",JSON.parse(data.body));
      JSON.parse(data.body).forEach( sale => {
        this.WooCommerce.getAsync("products/"+ sale.product_id).then(product => {
          var tempProduct = JSON.parse(product.body);
          tempProduct.count = 1;
          this.storage.get("individual-fav").then((val) => {
            val.forEach(element => {
              if(element.id == tempProduct.id)
                sale.favorit = true;
            });
            temp.push(tempProduct);
            if(temp.length == JSON.parse(data.body).length){
              this.storage.set('most-sale-cashe',{date: new Date(), products: temp});
              callback(false, temp);
            }
          });
        });      
      } );
      // }
    }, (err) => {
      callback(true, []);
      console.log(err);
    })
  }

  getMostSales( callback ){
    console.log("started");
      let temp = [];
      this.storage.get('most-sale-cashe').then(val => {
        if(val){
          console.log("got in storage");
          if(differenceInDays(val.date, new Date()) > -7){
            console.log("same day");
            temp = val.products;
            callback(false, temp);
          }else{
            console.log("differnt day");
            this.reachMostSales(function(err, products){
              callback(err, products);
            });
          } 
        }else{
          console.log("not found in storage");
          this.reachMostSales(function(err, products){
            callback(err, products);
          });
        }
      });
      
  };


   getNewProducts(callback){
    let temp = [];

    this.http.post("http://89.42.209.153:3002/getRecentProducts", {},{}).then(data => {
      console.log("HTTP",JSON.parse(data.data));
      if(JSON.parse(data.data).err){
        console.log(JSON.parse(data.data.err));
        callback(true, temp);
      }
        
      else{
        temp = JSON.parse(data.data).products;
        callback(false, temp);
      }

    });
    // this.WooCommerce.getAsync("products").then((data) => {
    //   console.log("new Products",JSON.parse(data.body));
    //   callback(false, JSON.parse(data.body));

    //   JSON.parse(data.body).forEach( product => {
    //     product.count = 1;
    //     this.storage.get("individual-fav").then((val) => {
    //       val.forEach(element => {
    //         if(element.id == product.id)
    //           product.favorit = true;
    //         temp.push(product);
    //       });
         
    //     });
    //     if(JSON.parse(data.body).length == temp.length){
    //       callback(false, temp);
    //     }
    //   });
    //   // this.loader.dismiss();

    // }, (err) => {
    //   console.log(err);
    //   callback(true, []);
    // });
  };

  getProducts(id, page, callback){
    let temp = [];

    this.http.post("http://89.42.209.153:3002/getProducts", {per_page: 25, page: page},{}).then(data => {
      console.log("HTTP",JSON.parse(data.data));
      if(JSON.parse(data.data).err){
        console.log(JSON.parse(data.data.err));
        callback(true, temp);
      }
        
      else{
        temp = JSON.parse(data.data).products;
        callback(false, temp);
      }

    });
  };


  getProductsById(id, page, callback){
    let temp = [];

    this.http.post("http://89.42.209.153:3002/getProductsById", {per_page: 25, page: page, id: id},{}).then(data => {
      console.log("HTTP",JSON.parse(data.data));
      if(JSON.parse(data.data).err){
        console.log(JSON.parse(data.data.err));
        callback(true, temp);
      }
        
      else{
        temp = JSON.parse(data.data).products;
        callback(false, temp);
      }

    });
  };

  searchProducts(name, page, callback){
    let temp = [];

    this.http.post("http://89.42.209.153:3002/searchProducts", {per_page: 25, page: page, name: name},{}).then(data => {
      console.log("HTTP",JSON.parse(data.data));
      if(JSON.parse(data.data).err){
        console.log(JSON.parse(data.data.err));
        callback(true, temp);
      }
        
      else{
        temp = JSON.parse(data.data).products;
        callback(false, temp);
      }

    });
  };

  getProduct(id, callback){
    this.http.post("http://89.42.209.153:3002/getProduct", {id: id},{}).then(data => {
      if(JSON.parse(data.data).err){
        callback(true, {});
      }else{
        callback(false, JSON.parse(data.data).product);
      }
    });
  };




  /***************************** */
  /*****CONNECTIVITY SECTION**** */
  /***************************** */
  checkConnection() {
    var networkState = this.network.type;

    if(networkState === 'none'){
      // this.showToastCancel("لطفا دسترسی به اینترنت را فعال کنید");
      
    }

    this.network.onDisconnect().subscribe(() => {
      this.showToastCancel("لطفا دسترسی به اینترنت را فعال کنید");
    });

    this.network.onConnect().subscribe(() => {
      // We just got a connection but we need to wait briefly
       // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        this.showToast("دسترسی به اینترنت وجود دارد");
        this.getCategories();
        // if (this.network.type === 'wifi') {
        //   console.log('we got a wifi connection, woohoo!');
        // }
        this.appUpdate.checkAppUpdate('http://89.42.209.153:3002/version').then((data) => { });
      }, 3000);
    });
  }

}

import { MainProvider } from './../providers/main/main';
import { Events } from 'ionic-angular/util/events';
import { SuperMarketPage } from './../pages/super-market/super-market';
import { TabsPage } from './../pages/tabs/tabs';
import { WizardPage } from './../pages/wizard/wizard';
import { CartPage } from './../pages/cart/cart';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { Storage } from '@ionic/storage';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import * as WC from 'woocommerce-api';
import { Network } from '@ionic-native/network';

@Component({
  templateUrl: 'app.html',
  providers: [MainProvider]
})
export class MyApp {
  WooCommerce: any;
  rootPage:any =   TabsPage;
  some:any;
  loggedIn         = false;
  loginPage       = LoginPage;
  categoriesPage  = SuperMarketPage;
  WizardPage      = WizardPage;
  cartPage        = CartPage;
  showedAlert      = false;
  net_problem     = true;

  @ViewChild(Nav) nav: Nav;
  constructor(public platform: Platform, public menu: MenuController, public statusBar: StatusBar, public splashScreen: SplashScreen, public storage: Storage, public actionSheetCtrl: ActionSheetController, public modalCtrl : ModalController, public events: Events, public network: Network) {
    // this.splashScreen.show();
    // platform.ready().then(() => {
    //   // Okay, so the platform is ready and our plugins are available.
    //   // Here you can do any higher level native things you might need.
    //   statusBar.styleDefault();
    //   splashScreen.hide();
    // });
    this.WooCommerce = WC({
      url: 'http://koomarket.com',
      consumerKey: 'ck_1e8772d1f9210198b612f773d9dc69045fe7b2ad',
      consumerSecret: 'cs_0278af86e0a9840d32b292d4dc20356666eca337',
      wpAPI: true,
      version: 'wc/v2'
    });
    this.initializeApp();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

    //   this.platform.registerBackButtonAction(() => {
    //     if (this.nav.canGoBack) {
    //         if (!this.showedAlert) {
    //             this.isItOkay();
    //             this.showedAlert = true;
    //         } /*else {
    //             this.showedAlert = false;
    //             this.confirmAlert.dismiss();
    //         }*/
    //     }

    //     this.nav.pop();
    // });
    // this.splashScreen.hide();
    this.statusBar.styleDefault();
    this.storage.ready().then(() => {
      // this.storage.set('category-cashe', {})
      // this.storage.set('most-sale-cashe',{});
      

      this.storage.get('logged').then(data => {
        if(data)
          this.loggedIn = data;
        else{
          this.loggedIn = false;
        }
      });

      this.storage.get('individual-fav').then((val) => {
        console.log("individuals", val);
        if(!val)
          this.storage.set('individual-fav', []);
      });

      this.storage.get('cartItems').then((val) => {
        console.log("cartItems", val);
        if(!val)
          this.storage.set('cartItems', []);
      });

      this.storage.get('cardIDs').then((val) => {
        if(!val)
          this.storage.set('cardIDs', [])
      });

      this.storage.get('wizard').then((val) => {
        console.log("wizard",val);
        if(val){
          // let splash = this.modalCtrl.create(SplashPage);
          // splash.present()
          console.log("true");
          // this.nav.setRoot(TabsPage);
          this.splashScreen.hide();
        }else{          
          this.storage.set('groups', [{img: "http://lorempixel.com/128/128/food/", title: "Fried Potato", subTitle: "Best ever",products:[]}]);
          // this.nav.setRoot(this.WizardPage);
          this.splashScreen.hide();
          
        }
      });
      });
    });
  }
  openPage( page ){
    this.nav.push(page);
  }

  isItOkay(){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'آیا می خواهید خارج شوید؟',
      buttons: [
        {
          text: 'بله',
          handler: () => {
            navigator['app'].exitApp();
          }
        },{
          text: 'خیر',
          role: 'cancel',
          handler: () => {
            this.showedAlert = false;
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }  


  gotoHome(){
    this.nav.push(this.rootPage, {tab : 0});
  }

  gotoCat(){
    this.nav.push(this.rootPage, {tab : 1});
  }

  gotoCart (){
    this.nav.push(this.rootPage, {tab : 2});
  }

  gotoSearch (){
    this.nav.push(this.rootPage, {tab : 3});
  }
  gotoFav(){
    this.nav.push(this.rootPage, {tab : 3});
  }

}

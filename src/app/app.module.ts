import { HTTP } from '@ionic-native/http';
// import { ReviewsPage } from './../pages/reviews/reviews';
import { ProductTabsPage } from './../pages/product-tabs/product-tabs';
// import { ProductInformationPage } from './../pages/product-information/product-information';
import { MorePage } from './../pages/more/more';
// import { GroupPage } from './../pages/group/group';
import { WizardPage } from './../pages/wizard/wizard';
import { AddAddressPage } from './../pages/add-address/add-address';
import { SearchPage } from './../pages/search/search';
import { RegisterPage } from './../pages/register/register';
import { LoginPage } from './../pages/login/login';
import { ProductPage } from './../pages/product/product';
import { CreateGroupeModalPage } from './../pages/create-groupe-modal/create-groupe-modal';
import { SuperMarketPage } from './../pages/super-market/super-market';
import { FruitsPage } from './../pages/fruits/fruits';
import { MainCategoriesPage } from './../pages/main-categories/main-categories';
import { FavoritesPage } from './../pages/favorites/favorites';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';import { Network } from '@ionic-native/network';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { SubCategoriesPage } from '../pages/sub-categories/sub-categories';
import { CartPage } from '../pages/cart/cart';

import { Ionic2RatingModule } from 'ionic2-rating';
import { AddressesPage } from '../pages/addresses/addresses';
import { HttpModule } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser'
import { Keyboard } from '@ionic-native/keyboard';
import { Dialogs } from '@ionic-native/dialogs';
import { MainProvider } from '../providers/main/main';
import { AppUpdate } from '@ionic-native/app-update';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    FavoritesPage,
    SubCategoriesPage,    
    CreateGroupeModalPage,
    CartPage,
    ProductPage,
    AddressesPage,
    LoginPage,
    RegisterPage,
    SearchPage,
    AddAddressPage,
    SuperMarketPage,
    WizardPage,
    FruitsPage,
    MainCategoriesPage,
    MorePage,
    ProductTabsPage,
    // SuperMarketPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      tabsHideOnSubPages: false,
    }),
    IonicStorageModule.forRoot(),
    Ionic2RatingModule, // Put ionic2-rating module here
    HttpModule
    ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    FavoritesPage,
    CreateGroupeModalPage,
    SubCategoriesPage,
    CartPage,
    ProductPage,
    AddressesPage,
    LoginPage,
    RegisterPage,
    SearchPage,
    AddAddressPage,
    SuperMarketPage,
    WizardPage,
    FruitsPage,
    MainCategoriesPage,
    MorePage,
    ProductTabsPage,
    // SuperMarketPage
  ],
  providers: [
    Keyboard,
    StatusBar,
    SplashScreen,
    Network,
    Dialogs,
    HTTP,
    AppUpdate,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser,
    MainProvider  ]
})
export class AppModule {}
